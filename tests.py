import json

import pytest

from app import app, db, Part

with open("fixtures.json", "r") as f:
    PARTS_FIXTURES = json.loads(f.read())


@pytest.fixture
def client():
    app.config["TESTING"] = True

    with app.app_context():
        db.create_all()
        client = app.test_client()

        yield client

        db.drop_all()


def populate_db():
    for part_attrs in PARTS_FIXTURES:
        part = Part(**part_attrs)
        db.session.add(part)
    db.session.commit()


def add_children(client):
    pass


def test_list_parts(client):
    populate_db()
    resp = client.get("/parts/")
    assert 200 <= resp.status_code < 300
    parts = json.loads(resp.data)
    for part in parts:
        try:
            orig_part = next(
                i for i in PARTS_FIXTURES if i["title"] == part["title"]
            )
        except StopIteration:
            orig_part = None
            pytest.fail("Part does not exist")
        del part["id"]
        del part["children"]
        assert orig_part == part


def test_create_part(client):
    new_part_data = {
        "title": "new_part",
        "props": {
            "color": "orange",
            "material": "plastic"
        }
    }
    resp = client.post("/parts/", json=new_part_data)
    assert 200 <= resp.status_code < 300
    new_part = json.loads(resp.data)
    del new_part["id"]
    del new_part["children"]
    assert new_part == new_part


def test_update_part(client):
    resp = client.post("/parts/", json=PARTS_FIXTURES[0])
    assert 200 <= resp.status_code < 300
    orig_part = json.loads(resp.data)
    orig_part_title = orig_part["title"]
    new_title = f"{orig_part_title}"
    updated_part = json.loads(client.patch("/parts/1/", json={"title": new_title}).data)
    assert updated_part["title"] == new_title


def test_delete_part(client):
    populate_db()
    resp = client.delete("/parts/1/")
    assert resp.status_code == 204


def test_add_children(client):
    populate_db()
    parts = json.loads(client.get("/parts/").data)

    top_level_assembly_id = parts[0]["id"]
    sub_assembly_id = parts[1]["id"]
    child_id_1 = parts[2]["id"]
    child_id_2 = parts[3]["id"]

    top_level_assembly_children = [sub_assembly_id, child_id_1]
    sub_assembly_children = [child_id_2]

    # Add the second and third parts as children of the top-level assembly
    resp = client.post(f"/parts/{top_level_assembly_id}/children/", json={"children": top_level_assembly_children})
    assert 200 <= resp.status_code < 300
    children = json.loads(resp.data)
    for child in children:
        assert child["id"] in top_level_assembly_children

    # Add the third part as child of the sub-assembly
    resp = client.post(f"/parts/{sub_assembly_id}/children/", json={"children": sub_assembly_children})
    assert 200 <= resp.status_code < 300
    children = json.loads(resp.data)
    for child in children:
        assert child["id"] in sub_assembly_children

    return top_level_assembly_id, top_level_assembly_children, sub_assembly_id, sub_assembly_children


def test_delete_children(client):
    parent_id, children_id, _, _ = test_add_children(client)

    # Delete the third part from children of the first part
    deleted_child = children_id.pop()
    resp = client.delete(f"/parts/{parent_id}/children/", json={"children": [deleted_child]})
    assert 200 <= resp.status_code < 300
    children = json.loads(resp.data)
    for child in children:
        assert child["id"] in children_id


def test_assemblies_list(client):
    top_level_assembly_id, _, sub_assembly_id, _ = test_add_children(client)

    # List all assemblies
    resp = client.get("/assemblies/")
    assert 200 <= resp.status_code < 300

    assemblies = json.loads(resp.data)
    assert len(assemblies) == 2
    for assembly in assemblies:
        assert assembly["id"] in (top_level_assembly_id, sub_assembly_id)


def test_top_assemblies_list(client):
    top_level_assembly_id, _, _, _ = test_add_children(client)

    # List all assemblies
    resp = client.get("/top-assemblies/")
    assert 200 <= resp.status_code < 300

    assemblies = json.loads(resp.data)
    assert len(assemblies) == 1
    assert assemblies[0]["id"] == top_level_assembly_id


def test_top_level_assemblies_list(client):
    _, _, sub_assembly_id, _ = test_add_children(client)

    # List all sub-assemblies
    resp = client.get("/sub-assemblies/")
    assert 200 <= resp.status_code < 300

    sub_assemblies = json.loads(resp.data)
    assert len(sub_assemblies) == 1
    assert sub_assemblies[0]["id"] == sub_assembly_id


def test_components_list(client):
    top_level_assembly_id, children_ids, sub_assembly_id, sub_assembly_children_ids = test_add_children(client)
    children_ids.extend(sub_assembly_children_ids)
    created_components = set()
    for child_id in children_ids:
        if child_id != top_level_assembly_id and child_id != sub_assembly_id:
            created_components.add(child_id)

    # List all components
    resp = client.get("/components/")
    assert 200 <= resp.status_code < 300

    components = json.loads(resp.data)
    assert len(components) == len(created_components)
    for component in components:
        assert component["id"] in created_components


def test_orphans_list(client):
    non_orphan_ids = set()
    part_1_id, part_1_children_ids, part_2_id, part_2_children_ids = test_add_children(client)
    non_orphan_ids.add(part_1_id)
    non_orphan_ids.add(part_2_id)
    for part_id in part_1_children_ids:
        non_orphan_ids.add(part_id)
    for part_id in part_2_children_ids:
        non_orphan_ids.add(part_id)

    parts = json.loads(client.get("/parts/").data)

    # List all orphan components
    resp = client.get("/orphans/")
    assert 200 <= resp.status_code < 300

    orphans = json.loads(resp.data)
    assert len(orphans) == len(parts) - len(non_orphan_ids)
    for orphan in orphans:
        assert orphan["id"] not in non_orphan_ids


def test_first_level_children_of_assembly(client):
    top_level_assembly_id, top_level_assembly_children, sub_assembly_id, sub_assembly_children = test_add_children(
        client
    )

    resp = client.get(f"/parts/{top_level_assembly_id}/")
    assert 200 <= resp.status_code < 300

    assembly = json.loads(resp.data)
    for child in assembly["children"]:
        assert child in top_level_assembly_children

    resp = client.get(f"/parts/{sub_assembly_id}/")
    assert 200 <= resp.status_code < 300

    assembly = json.loads(resp.data)
    for child in assembly["children"]:
        assert child in sub_assembly_children


def test_get_child_parents(client):
    top_level_assembly_id, top_level_assembly_children, sub_assembly_id, sub_assembly_children = test_add_children(
        client
    )
    parent_ids = (top_level_assembly_id, sub_assembly_id)

    resp = client.get(f"/parents/{next((child_id for child_id in sub_assembly_children))}/")
    assert 200 <= resp.status_code < 300
    parents = json.loads(resp.data)
    assert len(parents) == len(parent_ids)
    for parent in parents:
        assert parent["id"] in parent_ids
