## Setup:

### System-level dependencies:
- Python 3
- SQLite 3.* (compiled with [JSON1 extension](https://www.sqlite.org/json1.html))

### Project dependencies:
Create and activate a python virtual environment and install the dependencies:
- ```pip install -r requirements.txt```
- ```pip install -r requirements_dev.txt``` (for running the tests)

## Running tests:
Run `pytest` from the command line to run all the unit tests.

## Server:
Use `python app.py` to start the server. You can export `DEBUG=true` in your environment to run the server in debug mode.

## API endpoints:
Access the following endpoints at `localhost:5000`

| Endpoint                | Method  | Params                                                                  | Description                                                |
|------------------------ |-------- |------------------------------------------------------------------------ |----------------------------------------------------------- |
| `/parts/`               | GET     | -                                                                       | List all parts                                             |
| `/parts/`               | POST    |  `{ "title": <str: part title>, "props": <dict?: part properties> }`    | Create a new part                                          |
| `/parts/<id>/`          | GET     | -                                                                       | Get part details                                           |
| `/parts/<id>/`          | PATCH   |                                                                         | Update a part with the given params                        |
| `/parts/<id>/`          | DELETE  | -                                                                       | Delete a part                                              |
| `/parts/<id>/children/` | GET     | -                                                                       | List all children of a part                                |
| `/parts/<id>/children/` | POST    | `[<int: part_id>]`                                                      | Add the given parts in params as children of another part  |
| `/parts/<id>/children/` | DELETE  | `[<int: part_id>]`                                                      | Remove the given parts from children of a part             |
| `/assemblies/`          | GET     | -                                                                       | List all assemblies                                        |
| `/top-assemblies/`      | GET     | -                                                                       | List all top-level assemblies                              |
| `/sub-assemblies/`      | GET     | -                                                                       | List all sub-assemblies                                    |
| `/components/`          | GET     | -                                                                       | List all components                                        |
| `/orphans/`             | GET     | -                                                                       | List all orphan components                                 |
| `/parents/<id>/`        | GET     | `[<int: child_id>]`                                                     | List all direct and indirect parents of a given component  |
